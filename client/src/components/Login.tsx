import React from 'react';
import api from '../model/api';
import { ConnexionStatus } from '../common/apiTypes';
import { Link } from 'react-router-dom';

interface LoginState {
    incorrect: boolean;
    connected: boolean;
    username: string;
    password: string;
}

interface LoginProps {
    changeLoggedInState: (connected: boolean, username: string) => void;
}


export default class Login extends React.Component<LoginProps, LoginState> {
    constructor(props: LoginProps) {
        super(props);
        this.state = { incorrect: false, connected: false, username: '', password: '' };
    }

    componentDidMount() {
        // check if already connected
        api.isConnected().then(value => this.handleConnect(value, true));
    }

    handleConnect(result: ConnexionStatus, first = false) {
        // update the connexion status
        if (result.connected) {
            this.props.changeLoggedInState(true, result.username);
            this.setState({ connected: true, incorrect: false });
        } else
            this.setState({ incorrect: !first, connected: false });
    }

    onFormSubmit() {
        api.connect(this.state.username, this.state.password).then((value) => {
            this.handleConnect(value);
        });
    }

    render() {
        let status = '';
        if (this.state.incorrect) {
            status = 'Incorrect username/password';
        }
        return (<div>
            <h2>Log in</h2>
            <div id='loginStatus' className='fgred'>{status}</div>
            <form className='loginForm'>
                <label htmlFor='login'>
                    Username :
                </label>
                <input name='login' className='textInput' type='text' value={this.state.username} onChange={event => {
                    this.setState({ username: event.target.value });
                }} />

                <label htmlFor='password'>
                    Password :
                </label>
                <input name='password' className='textInput' type='password' value={this.state.password}
                    onChange={event => {
                        this.setState({ password: event.target.value });
                    }} />
                <button className='button' type='submit' onClick={(ev) => {
                    ev.preventDefault();
                    this.onFormSubmit();
                }}>Log in
                </button>
                <Link className='button' to='/register'>Register</Link>
            </form>
        </div>);
    }
}
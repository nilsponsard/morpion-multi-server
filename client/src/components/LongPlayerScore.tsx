import React from 'react';
import * as types from '../common/apiTypes';

interface LongPlayerScoreProps {
    infos: types.UserInfoResponse;
}

/**
 * show all available infos on the player
 */
const LongPlayerScore: React.FunctionComponent<LongPlayerScoreProps> = (props: LongPlayerScoreProps) => {
    if (props.infos.games <= 0) {
        return (<div>You played {props.infos.games} games</div>);
    }
    return (<div>
        You played {props.infos.games} games and won {props.infos.wins}
         ({
            Math.floor((props.infos.wins / props.infos.games) * 100)
        }%)
        of them.
    </div>);
};
export default LongPlayerScore;


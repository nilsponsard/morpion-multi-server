import React from 'react';
import { Redirect } from 'react-router-dom';
import api from '../model/api';
interface RevengeState {
    found: boolean;
    possible: boolean;
    searching: boolean;
}
/**
 * a component to wait for a revenge
 */
export default class Revenge extends React.Component<any, RevengeState>{
    constructor(props: any) {
        super(props);
        this.state = { found: false, possible: true, searching: false };
    }
    startSearching() {
        this.setState({ searching: true }, () => this.search());
    }
    search() {
        if (this.state.searching) {
            api.revenge().then(res => {
                if (!res.matchFound && res.possible) {
                    setTimeout(() => {
                        this.search();
                    }, api.delay);
                } else {
                    this.setState({ searching: false });
                }
                this.setState({ found: res.matchFound, possible: res.possible, searching: res.possible });
            }).catch(() => {
                setTimeout(() => {
                    this.search();
                }, api.delay);
            });
        }

    }
    render() {
        if (this.state.found)
            return (<Redirect to='/' />);
        if (!this.state.possible)
            return (<div>The opponent left</div>);
        if (this.state.searching)
            return (<div>Waiting for the opponent to respond ...</div>);
        return (<div>
            <button className='button' onClick={() => this.startSearching()}>Want revenge</button>
        </div >);
    }
}


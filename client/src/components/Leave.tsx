import React, { useEffect, useState } from 'react';
import { Redirect } from 'react-router-dom';

import api from '../model/api';

// just to leave the current game, then redirect to home 
const Leave: React.FunctionComponent = () => {
    const [stateReader, stateWriter] = useState(false);
    useEffect(() => {
        api.leaveGame()
            .then(() => {
                stateWriter(true);
            });
    });
    if (stateReader)
        return (<Redirect to='/home' />);
    return (<div>
        leaving ....
    </div>);
};
export default Leave;


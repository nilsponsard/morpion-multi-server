import React, { useEffect, useState } from 'react';
import { LeaderboardResponse, UserInfoResponse } from '../common/apiTypes';
import api from '../model/api';
import ShortPlayerScore from './ShortPlayerScore';

/**
 * function to show the leaderboard
 */
const Leaderboard: React.FunctionComponent = () => {
    const [reader, writer] = useState({ res: new LeaderboardResponse([]), done: false });
    useEffect(() => {
        if (!reader.done)
            api.getLeaderboard().then(res => {
                writer({ res, done: true });
            });
    });
    return (<div>
        <h2>Leaderboard</h2>
        {reader.res.arr.map((value, index) => {
            return (<div className='leaderboardEl' id={value.username}>
                <div>({index + 1}) {value.username}</div>
                <ShortPlayerScore infos={new UserInfoResponse(value.username, value.games, value.wins, value.points)} />
            </div>);
        })} </div>);
};
export default Leaderboard;



import React from 'react';
import { Redirect, Link } from 'react-router-dom';
import { UserInfoResponse } from '../common/apiTypes';
import LongPlayerScore from './LongPlayerScore';
import api from '../model/api';

interface HomeProps {
    username: string;
    lobby: string | undefined;
}
interface HomeState {
    status: string;
    found: boolean;
    playerInfos: UserInfoResponse;
    lobbyCreated: boolean;
    lobbyID: string;
}
export default class Home extends React.Component<HomeProps, HomeState> {
    constructor(props: HomeProps) {
        super(props);
        this.state = { status: '', found: false, playerInfos: new UserInfoResponse('', 0, 0, 0), lobbyCreated: false, lobbyID: '' };
    }
    /**
     * update the player infos
     */
    updateInfos() {
        api.getUserInfo(this.props.username)
            .then((playerInfos) => {
                this.setState({ playerInfos });
            });
    }
    componentDidMount() {
        // if we need to join a lobby
        if (this.props.lobby) {
            api.joinLobby(this.props.lobby).then(res => {
                if (res.matchFound) {
                    api.getGameStatus().then((value) => {
                        // join the game
                        if (value.success) this.setState({ found: true });
                    }).catch(err => {
                        this.setState({ found: false });
                    });
                }
            })
                .catch(() => { /* ignore the error */ });
        } else {
            // if we are already in a game
            api.getGameStatus().then((value) => {
                if (value.success) this.setState({ found: true });
            }).catch(err => {
                this.setState({ found: false });
            });
        }
    }
    createLobby() {
        api.createLobby()
            .then(res => {
                this.setState({ lobbyCreated: true, lobbyID: res.id });
                this.waitLobby();
            })
            .catch();
    }

    handleSearchClick() {
        this.setState({ status: 'Searching for an opponent' });
        api.searchMatch()
            .then(success => {
                if (success)
                    this.setState({ status: 'Opponent found', found: true });
                else {
                    if (api.searching)
                        this.setState({ status: 'An error happened, try to refresh the page' });
                }
            }).catch(r => {
                console.log("a", r);
                this.setState({ status: 'An error happened, try to refresh the page' });
            });
    }
    /**
     * wait for a user to join the lobby
     */
    waitLobby() {
        if (!this.state.found) {
            api.getGameStatus().then((value) => {
                if (value.success) this.setState({ found: true });
            }).catch(err => {
                setTimeout(() => {
                    this.waitLobby();
                }, api.delay);
            });
        }
    }
    handleStopClick() {
        api.stopSearch()
            .then(v => {
                this.setState({ status: 'You left the queue' });
            })
            .catch(r => {
                console.log("b", r);
                this.setState({ status: 'An error happened, try to refresh the page' });
            });
    }
    copyLinkToClipboard() {
        const input = document.getElementById('lobbyUrl') as HTMLInputElement;
        input.select();
        input.setSelectionRange(0, input.value.length);
        document.execCommand('copy');
    }

    render() {
        if (this.props.username !== this.state.playerInfos.username) this.updateInfos();
        if (this.state.found) // go to the game if we found it
            return (<Redirect to='/game' />);

        return (<div>
            <h2>Hello, {this.props.username} !</h2>
            <LongPlayerScore infos={this.state.playerInfos} />
            <Link id='leaderboardButton' to='/leaderboard' className='button' >Leaderboard</Link>
            <div>
                <div>{this.state.status}</div>
                {api.searching ? <button className='button' onClick={e => this.handleStopClick()} >Cancel search</button> :
                    <button className='button' onClick={e => { this.handleSearchClick(); }}>Search match</button>}
            </div>
            <div id='lobby'>
                {this.state.lobbyCreated ?
                    <div>
                        Invite link created :
                        <input id='lobbyUrl' type='text' className='textInput' value={window.location.origin + '/lobby/' + this.state.lobbyID} />
                        <button type='button' className='button' onClick={this.copyLinkToClipboard}>Copy to clipboard</button>
                    </div>
                    :
                    <button className='button' onClick={() => { this.createLobby(); }}>Create lobby</button>
                }
            </div>
        </div>);
    }
}
import React from 'react';
import api from '../model/api';
import { Link, Redirect } from 'react-router-dom';
import Grid, { playerColors } from './Grid';
import { UserInfoResponse, codes } from '../common/apiTypes';
import ShortPlayerScore from './ShortPlayerScore';
import Revenge from './Revenge';

interface GameProps {
    username: string;
}

interface GameState {
    opponent: string;
    opponentInfos: UserInfoResponse;
    playerInfos: UserInfoResponse;
    grid: Array<number>;
    turn: number;
    won: boolean;
    winner: number;
    playerNumber: number;
    disconnected: boolean;
    noMoreInGame: boolean;
}


export default class Game extends React.Component<GameProps, GameState> {
    constructor(props: GameProps) {
        super(props);
        this.state = {
            won: false, opponent: '?',
            opponentInfos: new UserInfoResponse('', 0, 0, 0),
            playerInfos: new UserInfoResponse('', 0, 0, 0),
            grid: [], turn: -1, winner: 0, playerNumber: 0, disconnected: false, noMoreInGame: false
        };
    }

    componentDidMount() {
        api.getUserInfo(this.props.username)
            .then((playerInfos) => {
                playerInfos.games -= 1; // remove current game
                this.setState({ playerInfos });
            });
        api.getOpponent()
            .then(res => {
                this.setState({ opponent: res.name });
                return api.getUserInfo(res.name);
            })
            .then(opponentInfos => {
                opponentInfos.games -= 1; // remove current game
                this.setState({ opponentInfos });
            });
        api.playerNumber()
            .then(res => {
                this.setState({ playerNumber: res.playerNumber });
            });

        this.update();
        // api.getGrid()
        //     .then(res => {
        //         this.setState({ grid: res.grid });
        //     });
    }
    /**
     * updates the game status
     * 
     * @param once don’t launch a timeout to do it regularly
     */

    update(once = false) {
        api.getGameStatus()
            .then(res => {
                // if something happened 
                if (res.turn !== this.state.turn || res.won || res.disconnected) {
                    api.getGrid().then(value => {
                        this.setState({
                            grid: value.grid,
                            turn: res.turn,
                            winner: res.winner,
                            won: res.won,
                            disconnected: res.disconnected
                        });
                    });
                }
                // if we need to schedule the update
                if (!res.won && !once)
                    setTimeout(() => {
                        this.update();
                    }, api.delay);

            })
            .catch((reason) => {
                if (reason.code && reason.code === codes.notInGame)
                    this.setState({ noMoreInGame: true });
                else if (!once)
                    setTimeout(() => {
                        this.update();
                    }, api.delay);
            });
    }

    // handle the click on an emplacement
    handleClick(i: number) {
        if (!this.state.won && !this.state.disconnected)
            api.place(i)
                .then(() => this.update(true))
                .catch(() => this.update(true));
    }

    render() {
        let gameStatus = '';
        let gameStatusClass = '';

        if (this.state.noMoreInGame)
            return (<Redirect to='/home' />);
        if (this.state.won) {
            if (this.state.winner === 0)
                gameStatus = 'Tie';
            else if (this.state.winner === this.state.playerNumber) {
                gameStatusClass = 'fggreen';
                gameStatus = 'You won';
            } else {
                gameStatusClass = 'fgred';
                gameStatus = 'You lost';
            }
        } else if (this.state.turn % 2 + 1 === this.state.playerNumber) {
            gameStatusClass = 'fggreen';
            gameStatus = 'Your turn';
        } else {
            gameStatus = 'Opponent’s turn';
            gameStatusClass = 'fgred';
        }
        if (this.state.disconnected) {
            gameStatus = 'The opponent left the game';
        }


        return (<div>
            <p><span
                className={'fg' + playerColors[this.state.playerNumber]}
            >{this.props.username}</span> (<ShortPlayerScore infos={this.state.playerInfos} />) VS
                <span className={'fg' + playerColors[this.state.playerNumber % 2 + 1]}> {this.state.opponent} </span>
                (<ShortPlayerScore infos={this.state.opponentInfos} />)</p>
            <Link className='button' to='/leave'>Leave game</Link>
            <div id='player'>You are playing : <div className={playerColors[this.state.playerNumber] + ' disc player'} />
            </div>
            <div id='gameStatus' className={gameStatusClass}>{gameStatus}</div>
            <div>
                <Grid playerNumber={this.state.playerNumber} arr={this.state.grid}
                    handleClick={(i: number) => this.handleClick(i)} />
            </div>
            <div>
                {this.state.won ? <Revenge /> : ''}
            </div>
        </div>);
    }
}
import React from 'react';
import { UserInfoResponse } from '../common/apiTypes';

interface ShortPlayerScoreProps {
    infos: UserInfoResponse;
}
const ShortPlayerScore: React.FunctionComponent<ShortPlayerScoreProps> = (props: ShortPlayerScoreProps) => {

    return (<span> {props.infos.wins}W {props.infos.games - props.infos.wins}L </span>);
};
export default ShortPlayerScore;


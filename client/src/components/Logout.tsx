import React from 'react';
import api from '../model/api';
import { Redirect } from 'react-router-dom';

interface LogoutState {
    done: boolean;
}
interface LogoutProps {

    changeLoggedInState: (connected: boolean, username: string) => void;
}
/**
 * logout then redirect to /login
 */
export default class Logout extends React.Component<LogoutProps, LogoutState> {
    constructor(props: LogoutProps) {
        super(props);
        this.state = { done: false };
    }
    componentDidMount() {
        api.disconnect()
            .then(() => {
                this.props.changeLoggedInState(false, '');
                this.setState({ done: true });
            })
            .catch((err) => {
                console.error(err);
            });
    }
    render() {
        if (this.state.done)
            return (<Redirect to='/login' />);
        return (<div>
            logging out ...
        </div>);
    }
}
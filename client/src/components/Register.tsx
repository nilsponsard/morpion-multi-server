import React from 'react';
import api from '../model/api';
import { Redirect, Link } from 'react-router-dom';

interface RegisterState {
    registered: boolean;
    firstRequest: boolean;
    error: boolean;
    username: string;
    passwordConfirm: string;
    password: string;
}

interface RegisterProps {
    changeLoggedInState: (connected: boolean, username: string) => void;
}

export default class Register extends React.Component<RegisterProps, RegisterState> {
    constructor(props: RegisterProps) {
        super(props);
        this.state = {
            registered: false,
            username: '',
            password: '',
            passwordConfirm: '',
            firstRequest: true,
            error: false
        };

    }
    // when clicking submit
    onFormSubmit() {
        // check if both passwords are the same
        if (this.state.password === this.state.passwordConfirm) {
            api.register(this.state.username, this.state.password).then((value) => {
                api.connect(this.state.username, this.state.password)
                    .then((res) => this.props.changeLoggedInState(res.connected, res.username))
                    .catch();
                this.setState({ registered: value.registered, firstRequest: false });
            }).catch(() => {
                this.setState({ error: true });
            });
        }
    }

    render() {
        let status = '';
        if (this.state.password !== this.state.passwordConfirm) {
            status = 'Passwords must match';
        }
        if (this.state.registered)
            return (<Redirect to='/login' />);

        else if (!this.state.firstRequest)
            status = 'Username already in use';

        if (this.state.error)
            status = 'Unknown error';
        return (<div>
            <h2>Register</h2>
            <div id='loginStatus' className='fgred'>{status}</div>
            <form className='loginForm'>
                <label htmlFor='username'>
                    Username :
                </label>
                <input name='username' className='textInput' type='text' value={this.state.username}
                    onChange={event => {
                        this.setState({ username: event.target.value });
                    }} />

                <label htmlFor='password'>
                    Password :
                </label>
                <input name='password' className='textInput' type='password' value={this.state.password}
                    onChange={event => {
                        this.setState({ password: event.target.value });
                    }} />
                <label htmlFor='passwordConfirm'>
                    Password confirmation :
                </label>
                <input name='passwordConfirm' className='textInput' type='password' value={this.state.passwordConfirm}
                    onChange={event => {
                        this.setState({ passwordConfirm: event.target.value });
                    }} />
                <button className='button' type='submit' onClick={(ev) => {
                    ev.preventDefault();
                    this.onFormSubmit();
                }}>Register
                </button>
                <Link className='button' to='/login'>Back</Link>
            </form>
        </div>);
    }
}
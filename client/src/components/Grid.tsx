import React from 'react';

interface GridProps {
    arr: Array<number>;
    handleClick: (i: number) => void;
    playerNumber: number;
}

export const playerColors = ['', 'yellow', 'red'];


const Grid: React.FunctionComponent<GridProps> = (props: GridProps) => {

    return (<div className='grid'>
        {props.arr.map((value, index) => {
            // compute the distance from the first row
            let cssProperties = { '--line': `-${Math.floor(index / 7)}rem` } as React.CSSProperties;

            return (<div key={index} className='square' onClick={() => props.handleClick(index)}>
                <div className={playerColors[value] + ' disc '} style={cssProperties} />
            </div>);
        })}
    </div>);
};
export default Grid;


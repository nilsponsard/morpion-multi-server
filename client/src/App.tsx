import React from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect, Link } from 'react-router-dom';
import './App.css';
import Login from './components/Login';
import Home from './components/Home';
import Logout from './components/Logout';
import Game from './components/Game';
import Leave from './components/Leave';
import Register from './components/Register';
import Leaderboard from './components/Leaderboard';
import api from './model/api';
interface AppState {
  connected: boolean;
  username: string;
  lobby: string | undefined;

}


export default class App extends React.Component<any, AppState> {

  constructor(props: any) {
    super(props);
    this.state = { connected: false, username: '', lobby: undefined };
  }

  changeLoggedInState(connected: boolean, username = '') {
    this.setState({ connected, username });
  }

  componentDidMount() {
    this.checkConnected();
  }

  checkConnected() {
    api.isConnected()
      .then((res) => {
        if (res.connected !== this.state.connected)
          this.setState({ connected: res.connected });

      })
      .catch((status) => {
        setTimeout(() => {
          this.checkConnected();
        }, 10000);
        console.error(status);
      });
  }

  render() {
    return (
      <div className="App" >
        <Router>
          <div id='header'>
            <Link to='/' className='siteTitle'>Connect four</Link>
            {this.state.connected ? <span className='headUsername'>{this.state.username}</span> : ''}
            {this.state.connected ? <Link className='button headLogout' to='/logout'>Log out</Link> : ''}

          </div>

          <Switch>
            <Route path="/login">
              {this.state.connected ? <Redirect to='/home' /> : <Login changeLoggedInState={(connected: boolean, username: string) => { this.changeLoggedInState(connected, username); }} />}
            </Route>
            <Route path='/home'>
              {this.state.connected ? <Home username={this.state.username} lobby={this.state.lobby} /> : <Redirect to='/login' />}
            </Route>
            <Route path='/game'>
              {this.state.connected ? <Game username={this.state.username} /> : <Redirect to='/login' />}
            </Route>
            <Route path='/leave'>
              {this.state.connected ? <Leave /> : <Redirect to='/login' />}
            </Route>
            <Route path='/logout' >
              <Logout changeLoggedInState={(connected: boolean, username: string) => { this.changeLoggedInState(connected, username); }} />
            </Route>
            <Route path='/register'>
              <Register changeLoggedInState={(connected: boolean, username: string) => { this.changeLoggedInState(connected, username); }} />
            </Route>
            <Route path='/leaderboard'>
              <Leaderboard />
            </Route>
            <Route path="/lobby/:id" component={(props: { match: { params: { id: string; }; }; }) => {
              this.setState({ lobby: props.match.params.id });
              return (<Redirect to='/' />);
            }} />
            <Route path="/">
              {this.state.connected ? <Redirect to='/home' /> : <Redirect to='/login' />}
            </Route>
          </Switch>

        </Router>

      </div >
    );
  }
}




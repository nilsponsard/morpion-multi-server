/**
 * @brief list of all available endpoints
 */

export const PREFIX = '/api/v1';
export const GET_IS_CONNECTED = '/connected';
export const POST_CONNECT = '/connect';
export const POST_LEAVE_QUEUE = '/queue/leave';
export const GET_SEARCH_MATCH = '/queue/search';
export const POST_DISCONNECT = '/disconnect';
export const GET_OPPONENT = '/game/opponent';
export const GET_GRID = '/game/grid';
export const GET_PLAYER_NUMBER = '/game/number';
export const GET_GAME_STATUS = '/game/status';
export const POST_GAME_PLACE = '/game/place';
export const POST_LEAVE_GAME = '/game/leave';
export const POST_REGISTER = '/register';
export const GET_USER_INFOS = '/user';
export const POST_CREATE_LOBBY = '/lobby/create';
export const GET_JOIN_LOBBY = '/lobby/join';
export const GET_REVENGE = '/revenge';
export const GET_LEADERBOARD = '/leaderboard';
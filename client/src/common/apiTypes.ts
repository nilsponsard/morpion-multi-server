/**
 * 
 * @brief Types shared between the client and the server, Requests are sent by the client and Responses are sent by the server
 */

/**
 * @brief the meaning of the http codes in the server respnonse
 * 
 */
export enum codes {
    success = 200,
    badRequest = 400,
    notConnected = 401,
    notInGame = 403,
    notFound = 404,
    serverError = 500
}

/**
 * Every server responses must extend this class
 */
export class StandardResponse {
    public success: boolean;
    public status: string | undefined;
    constructor(success = true, status?: string) {
        this.status = status;
        this.success = success;
    }
}

export class SimpleMessage {
    public message: string;
    constructor(message: string) {
        this.message = message;
    }
}

export class ErrorResponse extends StandardResponse {
    constructor(status: string) {
        super(false, status);
    }
}
export class ConnectedResponse extends StandardResponse {
    username: string;
    constructor(username = '') {
        super();
        this.username = username;
    }
}
export class ConnexionStatus extends StandardResponse {
    connected: boolean;
    username: string;
    constructor(connected: boolean, username = '') {
        super();
        this.connected = connected;
        this.username = username;
    }
}
export class SearchMatchResponse extends StandardResponse {
    matchFound: boolean;
    constructor(found: boolean) {
        super();
        this.matchFound = found;
    }
}



export class GetOpponentResponse extends StandardResponse {
    username: string;
    constructor(name: string) {
        super();
        this.username = name;
    }
}
export class GameInfoResponse extends StandardResponse {
    grid: Array<number>;
    opponentName: string;
    playerNumber: number;
    constructor(grid: Array<number>, opponentName: string, playerNumber: number) {
        super();
        this.grid = grid;
        this.opponentName = opponentName;
        this.playerNumber = playerNumber;
    }

}
export class GameGridResponse extends StandardResponse {
    grid: Array<number>;
    constructor(grid: Array<number>) {
        super();
        this.grid = grid;
    }
}

export class GamePlayerNumberResponse extends StandardResponse {
    playerNumber: number;
    constructor(playerNumber: number) {
        super();
        this.playerNumber = playerNumber;
    }
}
export class GameStatusResponse extends StandardResponse {
    turn: number;
    won: boolean;
    winner: number;
    disconnected: boolean;
    constructor(turn: number, won: boolean, winner: number, disconnected: boolean) {
        super();
        this.turn = turn;
        this.won = won;
        this.winner = winner;
        this.disconnected = disconnected;
    }
}

export class PlaceResponse extends StandardResponse {
    placed: boolean;
    constructor(placed: boolean) {
        super();
        this.placed = placed;
    }
}
export class RegisterResponse extends StandardResponse {
    registered: boolean; // false if users already exists

    constructor(registered: boolean) {
        super();
        this.registered = registered;
    }
}

export class UserInfoResponse extends StandardResponse {
    username: string;
    games: number;
    wins: number;
    points: number;

    constructor(username: string, games: number, wins: number, points: number) {
        super();
        this.username = username;
        this.games = games;
        this.wins = wins;
        this.points = points;
    }
}
export class CreateLobbyResponse extends StandardResponse {
    id: string;
    constructor(id: string) {
        super();
        this.id = id;
    }
}
export class RevengeResponse extends SearchMatchResponse {
    possible: boolean; // if the opponent left
    constructor(found: boolean, possible: boolean) {
        super(found);
        this.possible = possible;
    }
}

interface UserInfo {
    username: string, games: number, wins: number, points: number;
}
export class LeaderboardResponse extends StandardResponse {
    arr: Array<UserInfo>;
    constructor(arr: Array<UserInfo>) {
        super();
        this.arr = arr;
    }
}


export class ConnexionRequest {
    username: string;
    password: string;

    constructor(username: string, password: string) {
        this.username = username;
        this.password = password;
    }
}

/**
 * @brief request to place on the board
 */
export class PlaceRequest {
    pos: number;
    constructor(pos: number) {
        this.pos = pos;
    }
}
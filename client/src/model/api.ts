import * as types from '../common/apiTypes';
import * as urls from '../common/urls';

/**
 * @brief helper class to throw when an error happens
 */
export class ApiError {
    success = false;
    status: string;
    code: number;
    constructor(code: number, status = '') {
        this.status = status;
        this.code = code;
    }
}

/**
 * @brief interfaces with the different endpoints
 */
export class Api {
    searching: boolean;
    delay = 1000;
    constructor() {
        this.searching = false;
    }
    url(endPoint: string) {
        return urls.PREFIX + endPoint;
    }
    /**
     * @brief a helper function to do request on an endpoint
     * @param endPoint the specific endpoint
     * @param post if the request should be a POST (true) or a GET (false)
     * @param body the content of the request (POST only)
     * @returns a promise that returns <T> on success and throws an ApiError if success is false.
     */
    mFetch<T extends types.StandardResponse>(endPoint: string, post = false, body?: string) {
        return fetch(this.url(endPoint), {
            method: post ? 'POST' : 'GET',
            body: body
        })
            .then(async (response) => {
                if (response.status !== 200) {
                    let res = await response.json();
                    throw new ApiError(response.status, res.status);

                } else
                    return response.json();
            })
            .then(response => {
                return response as T;
            });

    }
    isConnected() {
        return this.mFetch<types.ConnectedResponse>(urls.GET_IS_CONNECTED)
            .then(res => {
                return new types.ConnexionStatus(true, res.username);
            })
            .catch(reason => {
                if (reason.code && reason.code === 401) {
                    return new types.ConnexionStatus(false, "");
                }
                throw reason;
            });
    }
    connect(username: string, password: string) {
        return this.mFetch<types.ConnexionStatus>(urls.POST_CONNECT, true, JSON.stringify(new types.ConnexionRequest(username, password)))
            .then(res => {
                return new types.ConnexionStatus(true, res.username);
            })
            .catch(reason => {
                if (reason.code && reason.code === 401) {
                    return new types.ConnexionStatus(false, "");
                }
                throw reason;
            });
    }
    disconnect() {
        return this.stopSearch()
            .then(() =>
                this.mFetch<types.StandardResponse>(urls.POST_DISCONNECT, true))
            .catch(() => this.mFetch<types.StandardResponse>(urls.POST_DISCONNECT, true));
    }

    private search(resolve: (value: boolean) => void) {
        if (this.searching)
            return this.mFetch<types.SearchMatchResponse>(urls.GET_SEARCH_MATCH)
                .then(value => {
                    if (value.matchFound) {
                        this.searching = false;
                        resolve(true);
                    }
                    else {
                        if (this.searching)
                            setTimeout(() => {
                                this.search(resolve);
                            }, this.delay);
                        else {
                            resolve(false);
                        }
                    }
                });
        resolve(false);
    }
    searchMatch() {
        this.searching = true;
        return new Promise<boolean>((resolve, reject) => {
            this.search(resolve)?.catch(reason => reject(reason));
        });
    }
    stopSearch() {
        this.searching = false;
        return this.mFetch<types.StandardResponse>(urls.POST_LEAVE_QUEUE, true);
    }
    getOpponent() {
        return this.mFetch<types.GetOpponentResponse>(urls.GET_OPPONENT);
    }
    getGrid() {
        return this.mFetch<types.GameGridResponse>(urls.GET_GRID);
    }
    getGameStatus() {
        return this.mFetch<types.GameStatusResponse>(urls.GET_GAME_STATUS);
    }
    place(i: number) {
        return this.mFetch<types.PlaceResponse>(urls.POST_PLACE, true, JSON.stringify(new types.PlaceRequest(i)));
    }
    playerNumber() {
        return this.mFetch<types.GamePlayerNumberResponse>(urls.GET_PLAYER_NUMBER);
    }
    leaveGame() {
        return this.mFetch<types.StandardResponse>(urls.POST_LEAVE_GAME, true);
    }
    register(username: string, password: string) {
        return this.mFetch<types.RegisterResponse>(urls.POST_REGISTER, true, JSON.stringify(new types.ConnexionRequest(username, password)));
    }
    getUserInfo(username: string) {
        return this.mFetch<types.UserInfoResponse>(urls.GET_USER_INFOS + '/' + username);
    }
    createLobby() {
        return this.mFetch<types.CreateLobbyResponse>(urls.POST_CREATE_LOBBY, true);
    }
    joinLobby(id: string) {
        return this.mFetch<types.SearchMatchResponse>(urls.GET_JOIN_LOBBY + '/' + id);
    }
    revenge() {
        return this.mFetch<types.RevengeResponse>(urls.GET_REVENGE);
    }
    getLeaderboard() {
        return this.mFetch<types.LeaderboardResponse>(urls.GET_LEADERBOARD);
    }

}
const api = new Api();

export default api;
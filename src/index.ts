import { Application, Router } from "https://deno.land/x/oak/mod.ts";
import { exists } from "https://deno.land/std@0.90.0/fs/exists.ts";
import { sessionRefresh } from './sessions.ts';
import * as api from "./api.ts";
import * as logger from './logger.ts';

const port = 8000;

const app = new Application({ keys: ["secret"] });

app.use(sessionRefresh);
app.use(async (ctx, next) => {
    await next();
    ctx.response.headers.append('Cache-Control', 'no-cache,must-revalidate');
    ctx.response.headers.append('Expires', 'Mon, 26 Jul 1997 05:00:00 GMT');
    ctx.response.headers.append('Access-Control-Allow-Origin', '*');
});
app.use(api.getRouter().routes());
app.use(async (ctx) => {
    const root = `${Deno.cwd()}/client/build/`;
    const wanted = ctx.request.url.pathname;
    let path = 'index.html';
    if (await exists(root + wanted)) path = wanted;

    await ctx.send({
        root,
        path: path,
        index: "index.html",
    }).catch((reason) => {
        logger.warning(path + " : " + reason, 'FileServe');
    });
});

app.addEventListener('listen', () => {
    logger.info(`Started listening on port ${port}`, 'index.ts');
    logger.debug(`Access it here : http://127.0.0.1:${port}`, 'index.ts');

});
app.listen(`0.0.0.0:${port}`);



import { sendJSONResponse } from './routeUtils.ts';
import { getSessionInfoCtx, SessionInfo } from './sessions.ts';
import { Game } from './games.ts';
import { ErrorResponse, codes } from '../client/src/common/apiTypes.ts';
import { Context } from 'https://deno.land/x/oak/mod.ts';
export function requireConnected(ctx: Context<Record<string, unknown>>, then: (session: SessionInfo) => unknown) {
    const session = getSessionInfoCtx(ctx);
    if (session)
        return then(session);
    sendJSONResponse(ctx, new ErrorResponse("not logged in"), codes.notConnected);
}

export function requireInGame(ctx: Context<Record<string, unknown>>, then: (session: SessionInfo, game: Game) => unknown) {
    return requireConnected(ctx, (session) => {
        if (session.user.currentGame)
            return then(session, session.user.currentGame);
        sendJSONResponse(ctx, new ErrorResponse('not in game'), codes.notInGame);
    });
}
import {rtContext} from './routeUtils.ts';
import {Context} from "https://deno.land/x/oak/mod.ts";
import {v4 as uuid} from 'https://deno.land/std@0.85.0/uuid/mod.ts';
import {leaveQueue} from './games.ts';
import {User} from './users.ts';
import {deleteLobby} from "./lobby.ts";

export async function sessionRefresh(ctx: Context<Record<string, unknown>>, next: () => Promise<void>) {
    const session = getSessionInfoCtx(ctx);
    if (session)
        session.pingReceived();

    await next();
}

export class SessionInfo {
    user: User;
    lastRequest: Date;

    constructor(user: User) {
        this.lastRequest = new Date();
        this.user = user;
    }

    pingReceived() {
        this.user.pingReceived();
        this.lastRequest.setTime(Date.now());
    }
}

export function getSessionInfoCtx(ctx: rtContext | Context<Record<string, unknown>>) {
    const id = ctx.cookies.get("session");
    if (id)
        return getSessionInfo(id);
    return undefined;

}

export function getSessionInfo(id: string) {
    return sessions.get(id);
}


export function createSession(ctx: rtContext, user: User) {
    const id = uuid.generate();
    sessions.set(id, new SessionInfo(user));
    ctx.cookies.set('session', id, {sameSite: 'strict'});
}

export function deleteSession(ctx: rtContext) {
    const session = getSessionInfoCtx(ctx);

    if (session) {
        leaveQueue(session.user);
        deleteLobby(session.user)
        if (session.user.currentGame)
            session.user.leaveGame();
        sessions.forEach((value, key) => {
            if (value === session) {
                sessions.delete(key)
                return
            }
        })

    }

}


const sessions: Map<string, SessionInfo> = new Map();
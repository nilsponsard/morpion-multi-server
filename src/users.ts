import { Game, leaveQueue } from './games.ts';
import * as logger from './logger.ts';
import { DB } from "https://deno.land/x/sqlite/mod.ts";
import { createHash } from "https://deno.land/std@0.91.0/hash/mod.ts";
import { deleteLobby } from './lobby.ts';
const connectedUsers: Map<string, User> = new Map();

// Open a database
const db = new DB("database/database.db");
db.query("CREATE TABLE IF NOT EXISTS users (username TEXT PRIMARY KEY, password TEXT, games INTEGER, wins INTEGER, points INTEGER)");


// Close connection
// db.close();


export class User {
    username: string;
    currentGame: Game | undefined;
    lastRequest: Date;
    createdLobby: boolean;
    _wins: number;
    _games: number;
    _points: number;

    constructor(username: string, games: number, wins: number, points: number) {
        this.lastRequest = new Date();
        this.username = username;
        this.createdLobby = false;
        this._games = games;
        this._wins = wins;
        this._points = points;
    }
    pingReceived() {
        this.lastRequest.setTime(Date.now());
    }
    joinGame(game: Game) {
        logger.debug(`User ${this.username} joined a game`);
        if (this.createdLobby)
            deleteLobby(this);

        this.games += 1;
        this.currentGame = game;
    }
    winGame() {
        this.wins += 1;
    }
    get points() {
        return this._points;
    }
    set points(n: number) {
        db.query('update users set points=? where username=?;', [n, this.username]);
        this._points = n;
    }
    get games() {
        return this._games;
    }
    set games(n: number) {
        db.query('update users set games=? where username=?;', [n, this.username]);
        this._games = n;
    }
    get wins() {
        return this._wins;
    }
    set wins(n: number) {
        db.query('update users set wins=? where username=?;', [n, this.username]);
        this._wins = n;
    }
    leaveGame() {
        if (this.currentGame)
            this.currentGame.leave(this);
        this.currentGame = undefined;
        logger.debug(`User '${this.username}' left the game`, 'users.ts');
    }
}

function hashPassword(password: string) {

    const hash = createHash("sha256");
    hash.update(password);
    return hash.toString('base64');
}
interface UserDBInfo { username: string, games: number, wins: number, points: number; }

export function getLeaderBoard() {
    const out: Array<UserDBInfo> = [];
    for (const obj of db.query('select username,games,wins,points from users order by wins desc limit ?', [10]).asObjects())
        out.push(obj as UserDBInfo);

    return out;
}

export function login(username: string, password: string) {
    for (const obj of db.query('select * from users where username=? and password=?', [username, hashPassword(password)]).asObjects()) {
        if (connectedUsers.has(username))
            return connectedUsers.get(username);
        return new User(username, obj.games, obj.wins, obj.points);
    }
    return undefined;
}

export function register(username: string, password: string) {
    for (const a of db.query("select * from users where username=? ", [username]))
        return false; // if the users exists 
    db.query("INSERT INTO users (username,password,games,wins,points) VALUES (?,?,?,?,?)", [username, hashPassword(password), 0, 0, 0]);
    logger.debug(`Registered user '${username}'`, 'users.ts');
    return true; // user successfully created
}
export function getUserInfo(username: string) {
    for (const obj
        of db.query('select username,games,wins,points from users where username=?', [username]).asObjects()) {
        return obj as { username: string, games: number, wins: number, points: number; };
    }
    return undefined;
}


function createDefaultAccounts() {
    register("sautax", "jesuissautax");
    register("user", "user");
    register("test", "test");

}
createDefaultAccounts();


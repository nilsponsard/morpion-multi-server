import { User } from './users.ts';
import { startGame } from './games.ts';
import { debug } from './logger.ts';

export function joinLobby(username: string, user2: User) {
    if (user2.username === username) return false;
    for (let index = 0; index < openLobbies.length; ++index) {
        if (openLobbies[index].username === username) {
            const user1 = openLobbies[index];
            if (user1.lastRequest.getTime() + 60 * 1000 < Date.now()) // if the user that created the lobby is inactive 
            {
                deleteLobby(user1);
                return false;
            }
            startGame(user1, user2);
            return true;
        }
    }
    return false;
}


export function createLobby(user: User) {
    if (openLobbies.indexOf(user) === -1) {
        debug(`User '${user.username}' created a lobby`, 'lobby.ts');
        user.createdLobby = true;
        openLobbies.push(user);
        return true;
    }
    return false;
}

export function deleteLobby(user: User) {
    for (let i = 0; i < openLobbies.length; ++i) {
        if (openLobbies[i] === user) {
            debug(`Deleted lobby of user '${user.username}'`, 'lobby.ts');
            openLobbies.splice(i, 1);
            user.createdLobby = false;
            return;
        }
    }
}


const openLobbies: Array<User> = [];
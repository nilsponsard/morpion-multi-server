import * as logger from './logger.ts';
import {User} from './users.ts';
import * as types from '../client/src/common/apiTypes.ts';


class Grid {
    columns = 7;
    rows = 6;
    won: boolean;
    array: Array<number>;
    turn: number;
    currentPlayer: number;
    winner: number;

    constructor() {
        this.won = false;
        this.turn = 0;
        this.array = [];
        this.winner = 0;

        /*
        with the css grid we will get this :
            0 1 2 
            3 4 5
            6 7 8
        */
        for (let i = 0; i < this.columns * this.rows; ++i) {
            this.array.push(0);
        }

        this.currentPlayer = this.getCurrentPlayer();
    }

    getCurrentPlayer() {
        return 1 + this.turn % 2;
    }

    /**
     *
     * @param pos the index of the square
     */
    checkWinCondition(pos: number) {
        let win = false;
        const player = this.array[pos];
        const column = pos % this.columns;
        const row = Math.floor(pos / this.columns);

        { // check horizontal
            let count = 0;
            let index = pos;
            //left
            while (index >= row * this.columns && this.array[index] === player) {
                count += 1;
                index -= 1;
            }
            //right
            index = pos + 1;
            while (index < (row + 1) * this.columns && this.array[index] === player) {
                count += 1;
                index += 1;
            }
            if (count >= 4) win = true;

        }
        if (!win) { // check vertical
            let count = 0;
            let index = pos;

            while (index < this.array.length && this.array[index] === player) {
                count += 1;
                index += this.columns;
            }
            if (count >= 4) win = true;

        }

        if (!win) { // check diagonal \
            let count = 0;
            let c = column;
            let r = row;
            // go bottom right
            while (r <= this.rows && c <= this.columns && this.array[r * this.columns + c] === player) {
                count += 1;
                r += 1;
                c += 1;
            }
            c = column - 1;
            r = row - 1;
            // go up left
            while (r >= 0 && c >= 0 && this.array[r * this.columns + c] === player) {
                count += 1;
                r -= 1;
                c -= 1;
            }

            if (count >= 4) win = true;
        }

        if (!win) { // check diagonal /
            let count = 0;
            let c = column;
            let r = row;
            // go bottom left
            while (r <= this.rows && c >= 0 && this.array[r * this.columns + c] === player) {
                count += 1;
                r += 1;
                c -= 1;
            }
            c = column + 1;
            r = row - 1;
            // go up right
            while (r >= 0 && c <= this.columns && this.array[r * this.columns + c] === player) {
                count += 1;
                r -= 1;
                c += 1;
            }

            if (count >= 4) win = true;
        }

        if (win) {
            this.end(this.getCurrentPlayer());
            return true;
        } else if (this.turn >= this.columns * this.rows - 1) {
            this.end(0);
            return true;
        }
    }

    end(player: number) {
        this.won = true;
        if (player > 0) {
            this.winner = player;
        } else {
            this.winner = 0;
        }
    }

    /**
     *
     * @param pos the index of the square
     */
    nextTurn(pos: number) {
        if (!this.checkWinCondition(pos)) {
            ++this.turn;
            this.currentPlayer = this.getCurrentPlayer();
        }
    }

    playPos(i: number) {
        const col = i % this.columns;

        if (this.array[col] === 0 && !this.won) { // if we can place it
            let index = col;
            while (index < this.array.length) {
                if (this.array[index] !== 0 || index > this.array.length)
                    break;
                index += this.columns;
            }
            this.array[index - this.columns] = this.getCurrentPlayer();
            this.nextTurn(index - this.columns);
            return true;
        } else
            return false;
    }
}


export class Game {

    revenge: [boolean, boolean];
    disconnected: [boolean, boolean];
    user1: User;
    user2: User;
    grid: Grid;

    constructor(user1: User, user2: User) {
        this.user1 = user1;
        this.user2 = user2;
        this.user1.joinGame(this);
        this.user2.joinGame(this);
        this.grid = new Grid();
        this.disconnected = [false, false];
        this.revenge = [false, false];
    }

    wantRevenge(user: User) {
        logger.debug(`User '${user.username}' wants revenge`, 'game.ts');
        if (!this.grid.won) return 2;
        if (this.disconnected[0] || this.disconnected[1]) return 0;
        if (user === this.user1) this.revenge[0] = true;
        if (user === this.user2) this.revenge[1] = true;
        if (this.revenge[0] && this.revenge[1]) {
            const user1 = this.user1;
            const user2 = this.user2;
            this.stop();
            startGame(user1, user2);
            return 2;
        }
        return 1;
    }

    applyWin() {
        if (this.grid.winner === 1 && !this.disconnected[0]) this.user1.winGame();
        if (this.grid.winner === 2 && !this.disconnected[1]) this.user2.winGame();
    }

    place(session: User, pos: number) {
        const player = this.grid.getCurrentPlayer();
        if (((session === this.user1 && player === 1) || (session === this.user2 && player === 2))
            && !(this.disconnected[0] || this.disconnected[1])) {
            const res = this.grid.playPos(pos);
            if (this.grid.won) this.applyWin();
            return res;
        } else
            return false;
    }

    leave(user: User) {
        if (this.user1 === user) {
            if (!this.disconnected[1] && !this.grid.won) // give a win to the other player if he leaves the game before the stop
                this.user2.winGame();
            this.disconnected[0] = true;
        } else if (this.user2 === user) {
            if (!this.disconnected[0] && !this.grid.won) // give a win to the other player if he leaves the game before the stop
                this.user1.winGame();
            this.disconnected[1] = true;
        }
    }

    getStatus() {
        // kick players after 1 min of inactivity
        const now = Date.now();
        if (this.user1.lastRequest.getTime() + 1 * 60 * 1000 < now)
            this.leave(this.user1);
        if (this.user2.lastRequest.getTime() + 1 * 60 * 1000 < now)
            this.leave(this.user2);

        new types.GameStatusResponse(
            this.grid.turn, this.grid.won, this.grid.winner, this.disconnected[0]
            || this.disconnected[1]);
    }

    stop() {
        this.user1.leaveGame();
        this.user2.leaveGame();
    }

}

const matchList: Array<Game> = [];

export function startGame(user1: User, user2: User) {
    if (user1.currentGame !== undefined || user2.currentGame !== undefined)
        return false; // can’t make a game if one of the user is already in a game
    if (Math.random() >= 0.5) // toss a coin
        matchList.push(new Game(user1, user2));
    else
        matchList.push(new Game(user2, user1));

    return true;
}

export function getOpponent(user: User) {
    if (user.currentGame)
        if (user.currentGame.user1 != user)
            return user.currentGame.user1;
        else return user.currentGame.user2;
    return undefined;
}


/**
 *
 * @param user
 * @returns true if a game is found
 */
export function searchMatch(user: User) {
    if (user.currentGame) return true;
    const now = Date.now();
    for (const player of playersSearching) {
        if (player.lastRequest.getTime() + 1000 * 10 < now) { // if a player has not pinged the server for the last 10 seconds
            logger.debug(`User '${player.username}' timed out`, 'game.ts');
            leaveQueue(player);
        } else if (player.username != user.username) {
            leaveQueue(user);
            leaveQueue(player);
            startGame(user, player);
            return true;
        }
    }
    if (playersSearching.indexOf(user) == -1) {
        playersSearching.push(user);
        logger.debug(`User '${user.username}' entered the queue`, 'game.ts');

    }

    return false;
}

export function leaveQueue(user: User) {
    const pos = playersSearching.indexOf(user);
    if (pos != -1) {
        logger.debug(`User '${user.username}' left the queue`, 'game.ts');
        playersSearching.splice(pos, 1);
        return true;
    }
    return false;
}

const playersSearching: Array<User> = [];
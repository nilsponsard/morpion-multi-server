import { Router, RouterContext } from "https://deno.land/x/oak/mod.ts";

import { sendJSONResponse, rtContext } from "./routeUtils.ts";
import * as types from '../client/src/common/apiTypes.ts';
import * as game from './games.ts';
import { createSession, getSessionInfoCtx, deleteSession } from "./sessions.ts";
import * as logger from './logger.ts';
import * as urls from '../client/src/common/urls.ts';
import { requireConnected, requireInGame } from './apiUtils.ts';
import * as users from './users.ts';
import * as lobby from './lobby.ts';


/**
 * GET
 * request parameters :
 * {}
 * response :
 * {
 *  "connected": true|false,
 *  "username" : string
 * }
 * connected is true on connexion success
 * 
 * 
 * @param ctx the context
 */
function isConnected(ctx: rtContext) {
    requireConnected(ctx, (session) => {
        const response = new types.ConnectedResponse(session.user.username);
        sendJSONResponse(ctx, response);
    });
}


/**
 * POST
 * request parameters :
 * {
 *  "username":"<username>",
 *  "password":"<password>"
 * }
 * response :
 * {
 *  "connected": true|false,
 *  "username" : string
 * }
 * 
 * connected is true on connexion success
 * 
 * 
 * @param ctx the context
 */

function connect(ctx: rtContext) {
    if (ctx.request.hasBody) {
        return ctx.request.body({ type: 'json' }).value
            .then((value: types.ConnexionRequest) => {
                const username = value.username;
                const password = value.password;
                if (username && password) {
                    const response = new types.ConnexionStatus(false);
                    const user = users.login(username, password);
                    if (user) {
                        response.connected = true;
                        response.username = username;
                        createSession(ctx, user);
                        logger.debug(`User '${username}' has connected`, 'api.ts');
                    }
                    sendJSONResponse(ctx, response);
                } else {
                    sendJSONResponse(ctx, new types.SimpleMessage('malformed request'), types.codes.badRequest);
                }
            }).catch(() => {

                sendJSONResponse(ctx, new types.SimpleMessage('unknown error'), types.codes.serverError);
            });
    } else {
        sendJSONResponse(ctx, new types.SimpleMessage('malformed request'), types.codes.badRequest);
    }

}

/**
 * GET
 * request parameters :
 * {
 * }
 * response :
 * {
 *  "matchFound": true|false
 * }
 * matchFound is true when a match is found or the player is already in a match
 * 
 * error if not connected
 * 
 * 
 * @param ctx the context
 */

function searchMatch(ctx: rtContext) {
    requireConnected(ctx, (session) => {
        sendJSONResponse(ctx, new types.SearchMatchResponse(game.searchMatch(session.user)));
    });
}

/**
 * POST
 * request parameters :
 * {
 * }
 * response :
 * {
 *      "success": true|false
 * }
 * success is true when the server managed to delete the element from the list
 * 
 * 
 * error if not connected
 * 
 * 
 * @param ctx the context
 */

function leaveQueue(ctx: rtContext) {
    requireConnected(ctx, (session) => {

        if (game.leaveQueue(session.user))
            ctx.response.status = 200;
        else
            sendJSONResponse(ctx, new types.SimpleMessage("not in queue"), 404);
    });
}

/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.GetOpponentResponse
 * 
 *  returns the name of the opponent
 * 
 * @param ctx the context
 */
function getOpponent(ctx: rtContext) {
    requireInGame(ctx, (session, _currentGame) => {
        const opponent = game.getOpponent(session.user);
        if (opponent)
            sendJSONResponse(ctx, new types.GetOpponentResponse(opponent.username));
        else sendJSONResponse(ctx, new types.SimpleMessage("Opponent not found"), types.codes.notFound);
    });
}

function disconnect(ctx: rtContext) {
    deleteSession(ctx);
    sendJSONResponse(ctx, new types.StandardResponse());
}

/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.GameGridResponse
 * 
 * return the grid of the game
 * 
 * @param ctx the context
 */
function getGrid(ctx: rtContext) {
    requireInGame(ctx, (_session, currentGame) => {
        sendJSONResponse(ctx, new types.GameGridResponse(currentGame.grid.array));
    });
}

/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.GameGridResponse
 * 
 * return the number assigned to the player
 * 
 * @param ctx the context
 */
function getPlayerNumber(ctx: rtContext) {
    requireInGame(ctx, (session, currentGame) => {
        let playerNumber = 1;
        if (currentGame.user2 === session.user)
            playerNumber = 2;
        sendJSONResponse(ctx, new types.GamePlayerNumberResponse(playerNumber));
    });
}

/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.GameStatusResponse
 * 
 * return the turn (it’s a player number)
 * this endpoint will be pinged every seconds to update the game’s state
 * 
 * @param ctx the context
 */
function getGameStatus(ctx: rtContext) {
    requireInGame(ctx, (_session, currentGame) => {
        sendJSONResponse(ctx, new types.GameStatusResponse(
            currentGame.grid.turn, currentGame.grid.won, currentGame.grid.winner, currentGame.disconnected[0]
        || currentGame.disconnected[1]));
    });
}


/**
 * POST
 * request parameters :
 * types.PlaceRequest
 * response :
 * types.GameGridResponse
 * 
 * return the current game grid, error if it’s not the user’s turn
 * 
 * @param ctx the context
 */
function place(ctx: rtContext) {
    requireInGame(ctx, (session, currentGame) => {
        ctx.request.body({ type: 'json' }).value
            .then((value: types.PlaceRequest) => {
                if (value.pos !== undefined)
                    sendJSONResponse(ctx, new types.PlaceResponse(currentGame.place(session.user, value.pos)));
                else sendJSONResponse(ctx, new types.SimpleMessage('Malformed request'), types.codes.badRequest);
            });
    });
}



/**
 * POST
 * request parameters :
 * response :
 * types.StandardResponse
 * 
 * make the user leave the game
 * 
 * @param ctx the context
 */
function leaveGame(ctx: rtContext) {
    requireInGame(ctx, (session, _currentGame) => {
        session.user.leaveGame();
        sendJSONResponse(ctx, new types.StandardResponse());
    });
}

/**
 * POST
 * request parameters :
 * types.ConnexionRequest
 * response :
 * types.RegisterResponse
 * 
 * registers a new user 
 * 
 * @param ctx the context
 */
function register(ctx: rtContext) {
    if (ctx.request.hasBody) {

        return ctx.request.body({ type: 'json' }).value
            .then((value: types.ConnexionRequest) => {

                const username = value.username;
                const password = value.password;
                if (username && password) {
                    const response = new types.RegisterResponse(users.register(username, password));
                    sendJSONResponse(ctx, response);
                } else {
                    sendJSONResponse(ctx, new types.SimpleMessage('malformed request'));
                }
            }).catch((err: unknown) => {
                logger.warning(`database error : ${JSON.stringify(err)}`, 'api.ts');

                sendJSONResponse(ctx, new types.SimpleMessage('unknown error'), types.codes.serverError);
            });
    } else {
        sendJSONResponse(ctx, new types.SimpleMessage('malformed request'), types.codes.badRequest);
    }
}

/**
 * GET /player/:username
 * request parameters :
 * :username : the username of the user
 * response :
 * types.UserInfoResponse
 * 
 * get the infos about an user
 * 
 * @param ctx the context
 */
function getUserInfo(ctx: RouterContext<{
    username: string;
}, Record<string, unknown>>) {
    if (ctx.params && ctx.params.username) {
        const username = ctx.params.username;
        const infos = users.getUserInfo(username);
        if (infos) {
            sendJSONResponse(ctx, new types.UserInfoResponse(infos.username, infos.games, infos.wins, infos.points));
        } else {
            sendJSONResponse(ctx, new types.SimpleMessage('Player not found'), types.codes.notFound);
        }
    } else {
        sendJSONResponse(ctx, new types.SimpleMessage('Malformed request'), types.codes.badRequest);
    }
}



/**
 * GET /join_lobby/:username
 * request parameters :
 * :username : the username of the user that created the lobby
 * response :
 * types.SearchMatchResponse
 * 
 * joins the lobby if available
 * 
 * @param ctx the context
 */
function joinLobby(ctx: RouterContext<{
    username: string;
}, Record<string, unknown>>) {
    requireConnected(ctx, (session) => {
        if (ctx.params && ctx.params.username) {
            const username = ctx.params.username;
            sendJSONResponse(ctx, new types.SearchMatchResponse(lobby.joinLobby(username, session.user)));
        } else {
            sendJSONResponse(ctx, new types.SimpleMessage('Malformed request'), types.codes.badRequest);
        }
    });
}

/**
 * POST
 * request parameters :
 * 
 * response :
 * types.CreateLobbyResponse
 * 
 * creates a lobby
 * 
 * @param ctx the context
 */
function createLobby(ctx: rtContext) {
    requireConnected(ctx, (session) => {
        lobby.createLobby(session.user);
        sendJSONResponse(ctx, new types.CreateLobbyResponse(session.user.username));
    });
}


/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.RevengeResponse
 * 
 * error if not in a game
 * 
 * @param ctx the context
 */
function revenge(ctx: rtContext) {
    requireInGame(ctx, (session, currentGame) => {
        let possible = false;
        let found = false;
        const result = currentGame.wantRevenge(session.user);
        if (result >= 1) possible = true;
        if (result >= 2) found = true;
        sendJSONResponse(ctx, new types.RevengeResponse(found, possible));
    });
}
/**
 * GET
 * request parameters :
 * {}
 * response :
 * types.RevengeResponse
 * 
 * error if not in a game
 * 
 * @param ctx the context
 */
function getLeaderboard(ctx: rtContext) {
    sendJSONResponse(ctx, new types.LeaderboardResponse(users.getLeaderBoard()));
}


export function getRouter() {

    const router = new Router();
    router.prefix(urls.PREFIX)
        .get(urls.GET_IS_CONNECTED, isConnected)
        .post(urls.POST_CONNECT, connect)
        .post(urls.POST_LEAVE_QUEUE, leaveQueue)
        .get(urls.GET_SEARCH_MATCH, searchMatch)
        .post(urls.POST_DISCONNECT, disconnect)
        .get(urls.GET_OPPONENT, getOpponent)
        .get(urls.GET_GRID, getGrid)
        .get(urls.GET_PLAYER_NUMBER, getPlayerNumber)
        .get(urls.GET_GAME_STATUS, getGameStatus)
        .post(urls.POST_GAME_PLACE, place)
        .post(urls.POST_LEAVE_GAME, leaveGame)
        .post(urls.POST_REGISTER, register)
        .get<{ username: string; }>(urls.GET_USER_INFOS + '/:username', getUserInfo)
        .get<{ username: string; }>(urls.GET_JOIN_LOBBY + '/:username', joinLobby)
        .post(urls.POST_CREATE_LOBBY, createLobby)
        .get(urls.GET_REVENGE, revenge)
        .get(urls.GET_LEADERBOARD, getLeaderboard);

    return router;
}